﻿using Microsoft.AspNetCore.Mvc;
using TodoApp.Data;
using TodoApp.Models;

namespace TodoApp.Controllers;

[ApiController]
[Route("api/[controller]")]
public class TodoItemsController : Controller
{
    TodoItemsRepository todoItemRepository = new TodoItemsRepository(Environment.GetEnvironmentVariable("DB_CONNECTION_STRING"));

        [HttpGet]
        [ProducesResponseType(200, Type = typeof(IEnumerable<TodoItem>))]
        public IActionResult Get()
        {
            var todoItems = todoItemRepository.Get();

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            return Ok(todoItems);
        }

        [HttpGet("{todoId}")]
        [ProducesResponseType(200, Type = typeof(TodoItem))]
        [ProducesResponseType(400)]
        public IActionResult Get(int todoId)
        {

            TodoItem toDoItem = todoItemRepository.Get(todoId);

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            return Ok(toDoItem);
        }

        [HttpPost]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        public IActionResult CreateToDoItem([FromBody] TodoItem todo)
        {
            if (todo == null)
                return BadRequest(ModelState);

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            bool isTodoSaved = todoItemRepository.Create(todo);
            
            if (isTodoSaved == true) 
            {
                return Ok("Successfully Saved");
            } else {
                return BadRequest("Not Saved!");
            }
        }

        [HttpPut("{todoId}")]
        [ProducesResponseType(205)]
        [ProducesResponseType(400)]
        public IActionResult Update(int todoId, [FromBody] TodoItem todo)
        {
            if (todo == null || todo.Id != todoId)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            bool isUpdated = todoItemRepository.Update(todoId, todo);
            
            if (isUpdated == true)
                return Ok("Todo Updated Successfully");
            else
                return BadRequest("There was some error updating the Todo Item");
        }

        [HttpDelete("{todoId}")]
        [ProducesResponseType(206)]
        [ProducesResponseType(400)]
        public IActionResult DeleteToDoItem(int todoId)
        {
            bool isDeleted = todoItemRepository.Delete(todoId);
            
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            if (isDeleted == true)
                return Ok("Todo Delete Successfully");
            else
                return BadRequest("There was a problem deleting the Todo Item");
        }


}
