using TodoApp.Models;
using Microsoft.AspNetCore.Mvc;
using TodoApp.Data;
namespace Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : Controller
    {
        UsersRepository usersRepository = new UsersRepository(Environment.GetEnvironmentVariable("DB_CONNECTION_STRING"));

        [HttpGet("{userId}")]
        [ProducesResponseType(200, Type = typeof(User))]
        [ProducesResponseType(400)]
        public IActionResult Get(int userId)
        {
            User user = usersRepository.Get(userId);

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            return Ok(user);
        }

        [HttpPost]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        public IActionResult Create([FromBody] User user)
        {
            if (user == null)
                return BadRequest(ModelState);

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            bool isUserSaved = usersRepository.Create(user);
            
            if (isUserSaved) 
            {
                return Ok("User Successfully Saved");
            } else {
                return BadRequest("Not Saved");
            }
        }

        [HttpPut("{userId}")]
        [ProducesResponseType(205)]
        [ProducesResponseType(400)]
        public IActionResult Update(int userId, [FromBody] User user)
        {
            if (user == null || user.Id != userId)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            bool isUpdated = usersRepository.Update(userId, user);
            
            if (isUpdated)
                return Ok("User Updated Successfully");
            else
                return BadRequest("There was some error updating the User Item");
        }

        [HttpDelete("{userId}")]
        [ProducesResponseType(206)]
        [ProducesResponseType(400)]
        public IActionResult Delete(int userId)
        {
            bool isDeleted = usersRepository.Delete(userId);
            
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            if (isDeleted)
                return Ok("User Deleted Successfully");
            else
                return BadRequest("There was a problem deleting the User Item");
        }
    }
}