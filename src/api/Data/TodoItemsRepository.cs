﻿using TodoApp.Models;
using System.Data.SqlClient;
using TodoApp.Interfaces;

namespace TodoApp.Data;
public class TodoItemsRepository : ITodoItemsRepository
{
    private readonly string _connectionString;

    public TodoItemsRepository(string connectionString)
    {
        _connectionString = connectionString;
    }

    public ICollection<TodoItem> Get()
    {
        ICollection<TodoItem> todoItems = new List<TodoItem>();
        try
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                // Creating the command object
                SqlCommand cmd = new SqlCommand("select * from Hardik.TodoList", connection);
                // Opening Connection  
                connection.Open();
                // Executing the SQL query  
                SqlDataReader sdr = cmd.ExecuteReader();
                //Looping through each record
                while (sdr.Read())
                {
                    TodoItem todoItem = new TodoItem()
                    {
                        Id = (int)sdr["id"],
                        TaskName = (string)sdr["TaskName"],
                        WasCreatedOn = (DateTime) sdr["WasCreatedOn"],
                        IsCompleted = (bool) sdr["IsComplete"],
                        UserId = (int) sdr["UserId"]
                    };

                    todoItems.Add(todoItem);
                }
            }
        }
        catch (Exception e)
        {
            Console.WriteLine("OOPs, something went wrong.\n" + e);
        }

        return todoItems;
    }

    public TodoItem Get(int id)
    {
        TodoItem todoItem = new TodoItem();
        try
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                // Creating the command object
                SqlCommand cmd = new SqlCommand($"select * from Hardik.TodoList WHERE Id={id}", connection);
                // Opening Connection  
                connection.Open();
                // Executing the SQL query  
                SqlDataReader sdr = cmd.ExecuteReader();

                if (sdr.Read())
                {
                    todoItem = new TodoItem()
                    {
                        Id = (int)sdr["id"],
                        TaskName = (string)sdr["TaskName"],
                        WasCreatedOn = (DateTime) sdr["WasCreatedOn"],
                        IsCompleted = (bool) sdr["IsComplete"],
                        UserId = (int) sdr["UserId"]
                    };
                } else
                {
                    Console.WriteLine($"There is no Todo avaible with ID {id}");
                }
            }
        }
        catch (Exception e)
        {
            Console.WriteLine("OOPs, something went wrong.\n" + e);
        }

        return todoItem;
    }

    public bool Create(TodoItem todo)
    {
        try
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                // Creating the command object
                string query = "INSERT INTO Hardik.TodoList (TaskName, WasCreatedOn, UserId, IsComplete) VALUES (@TaskName, @WasCreatedOn, @UserId, @IsCompleted)";
                SqlCommand command = new SqlCommand(query, connection);
                command.Parameters.AddWithValue("@TaskName", todo.TaskName);
                command.Parameters.AddWithValue("@WasCreatedOn", todo.WasCreatedOn);
                command.Parameters.AddWithValue("@UserId", todo.UserId);
                command.Parameters.AddWithValue("@IsCompleted", todo.IsCompleted);
                
                connection.Open();
                
                int rowsAdded = command.ExecuteNonQuery();

                if (rowsAdded > 0)
                {
                    return true;
                } else
                {
                    return false;
                }
            }
        }
        catch (Exception e)
        {
            Console.WriteLine("OOPs, something went wrong.\n" + e);
            return false;
        }
        
    }

    public bool Update(int id, TodoItem todo)
    {
            try
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                // Creating the command object
                string query = "UPDATE Hardik.TodoList " +
                   "SET TaskName = @TaskName, " +
                   "    WasCreatedOn = @WasCreatedOn, " +
                   "    UserId = @UserId, " +
                   "    IsCompleted = @IsCompleted " +
                   "WHERE Id = @Id";

                SqlCommand command = new SqlCommand(query, connection);
                command.Parameters.AddWithValue("@TaskName", todo.TaskName);
                command.Parameters.AddWithValue("@WasCreatedOn", todo.WasCreatedOn);
                command.Parameters.AddWithValue("@UserId", todo.UserId);
                command.Parameters.AddWithValue("@IsCompleted", todo.IsCompleted);
                command.Parameters.AddWithValue("@Id", todo.Id); 

                connection.Open();
                int rowsUpdated = command.ExecuteNonQuery();


                if (rowsUpdated > 0)
                {
                    return true;
                } else
                {
                    return false;
                }
            }
        }
        catch (Exception e)
        {
            Console.WriteLine("OOPs, something went wrong.\n" + e);
            return false;
        }
    }


    public bool Delete(int id)
    {
        try
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                // Creating the command object
                
                SqlCommand command = new SqlCommand($"Delete from Hardik.TodoList Where id={id}", connection);
                connection.Open();
                int rowsDeleted = command.ExecuteNonQuery();

                if (rowsDeleted > 0)
                {
                    return true;
                } else
                {
                    return false;
                }
            }
        }
        catch (Exception e)
        {
            Console.WriteLine("OOPs, something went wrong.\n" + e);
            return false;
        }
    }

}
