using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TodoApp.Models;
using System.Data.SqlClient;
using TodoApp.Interfaces;

namespace TodoApp.Data;
public class UsersRepository : IUsersRepository
{
    private readonly string _connectionString;

    public UsersRepository(string connectionString)
    {
        _connectionString = connectionString;
    }
    public User Get(int id)
    {
        User user = new User();
        try
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                // Creating the command object
                SqlCommand cmd = new SqlCommand($"select * from Hardik.Users WHERE Id={id}", connection);
                // Opening Connection  
                connection.Open();
                // Executing the SQL query  
                SqlDataReader sdr = cmd.ExecuteReader();

                if (sdr.Read())
                {
                    user.Id = (int) sdr["id"];
                    user.FullName = (string) sdr["FullName"];
                    user.Email = (string) sdr["Email"];
                    user.PasswordHash = (string) sdr["PasswordHash"];

                } else
                {
                    Console.WriteLine($"There is no Todo avaible with ID {id}");
                }
            }
        }
        catch (Exception e)
        {
            Console.WriteLine("OOPs, something went wrong.\n" + e);
        }

        return user;
    }

    public bool Create(User user)
    {
        TodoItem todoItem = new TodoItem();
        try
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                // Creating the command object
                SqlCommand cmd = new SqlCommand($"INSERT INTO Hardik.Users (FullName, Email, PasswordHash) VALUES ('{user.FullName}','{user.Email}', '{user.PasswordHash}')", connection);
                // Opening Connection  
                connection.Open();
                // Executing the SQL query  
                int rowAdded = cmd.ExecuteNonQuery();

                if (rowAdded > 0)
                {
                    return true;
                } else
                {
                    return false;
                }
            }
        }
        catch (Exception e)
        {
            Console.WriteLine("OOPs, something went wrong.\n" + e);
            return false;
        }
        
    }

    public bool Update(int id, User user)
    {
            try
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                // Creating the command object
                    SqlCommand command = new SqlCommand($"Update Hardik.Users Set FullName ='{user.FullName}', Email ='{user.Email}', PasswordHash='{user.PasswordHash}' Where id={id}", connection);

                connection.Open();
                int rowsUpdated = command.ExecuteNonQuery();


                if (rowsUpdated > 0)
                {
                    return true;
                } else
                {
                    return false;
                }
            }
        }
        catch (Exception e)
        {
            Console.WriteLine("OOPs, something went wrong.\n" + e);
            return false;
        }
    }

    public bool Delete(int id)
    {
        try
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                // Creating the command object
                
                SqlCommand command = new SqlCommand($"Delete from Hardik.Users Where id={id}", connection);
                connection.Open();
                int rowsDeleted = command.ExecuteNonQuery();

                if (rowsDeleted > 0)
                {
                    return true;
                } else
                {
                    return false;
                }
            }
        }
        catch (Exception e)
        {
            Console.WriteLine("OOPs, something went wrong.\n" + e);
            return false;
        }
    }


    
}
