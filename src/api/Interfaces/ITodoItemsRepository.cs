﻿using TodoApp.Models;

namespace TodoApp.Interfaces;
public interface ITodoItemsRepository
{
	public ICollection<TodoItem> Get();
	
	public TodoItem Get(int id);

	public bool Create(TodoItem todo);

	public bool Update(int id, TodoItem todo);

	public bool Delete(int id);



}
