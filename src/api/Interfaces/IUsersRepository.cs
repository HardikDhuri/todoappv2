﻿using TodoApp.Models;

namespace TodoApp.Interfaces;
public interface IUsersRepository
{
	public User Get(int id);
	
	public bool Create(User user);

	public bool Update(int id, User user);

	public bool Delete(int id);

}
