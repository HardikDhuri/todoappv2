﻿using System.Data.SqlTypes;
using System.Data;
using System;

namespace TodoApp.Models;
public class TodoItem
{
    public int Id { get; set; }
    public string? TaskName { get; set; }
    public DateTime WasCreatedOn { get; set; }
    public int UserId { get; set; }
    public bool IsCompleted { get; set; }

}
