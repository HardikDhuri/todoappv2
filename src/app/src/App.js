import { useEffect, useState } from "react";
import AddButton from "./Components/AddButton";
import TaskInput from "./Components/TaskInput";
import Title from "./Components/Title";
import TodoList from "./Components/TodoList";
import Task from "./Task";
import Dropdown from "./Components/Dropdown";
import axios, { formToJSON } from "axios";

const baseUrl = "https://localhost:5001/api/ToDoItems"
function getFormatedTime() {
  const now = new Date();
  const hours = now.getHours().toString().padStart(2, '0');
  const minutes = now.getMinutes().toString().padStart(2, '0');
  const timeString = `${hours}:${minutes}`;
  return timeString; 
}


function App() {
  const taskTypes = ["personal", "work", "other"];
  const [toDos, setTodos] = useState([]);
  const [type, setType] = useState("personal");
  const [inputValue, setInputValue] = useState("");
  const [currId, setCurrId] = useState(null);

  useEffect(() => {
    
    axios.get(baseUrl)
      .then(response => {
        setTodos(response.data);
        console.log("Effect was called");
      })
      .catch(error => {
        console.error(error);
      });


  }, []);


  const handleInput = (event) => {
    setInputValue(event.target.value);
  };

  const handleChangeDropdown = (event) => {
    setType(event.target.value);
  };

  const handleEditTask = (id, text, type) => {
    setInputValue(text);
    setType(type);
    setCurrId(id);
    document.getElementById("toDoInput").focus();
  };

  const handleAddTask = () => {
    if (inputValue == false) {
      alert("Please add a todo before adding!");
    } else {
      let data = {
        "taskName": inputValue.toString(),
        "created": new Date().toISOString(),
        "type": type.toString()
      }
  
      if (currId != null) {
        data.id = currId;

        axios.put(`${baseUrl}/${currId}`, data)
          .then(response => {
            console.log(response.data);
          })
          .catch(error => {
            console.log(error);
          });
      } else {
        console.log("Going to post the data");

        axios.post(baseUrl, data)
          .then(response => {
            console.log(response.data);
          })
          .catch(error => {
            console.log(error);
          });
      }

      setInputValue("");
      setType("personal");
      setCurrId(null);
      setTodos(toDos);
    }
    
  };

  const handleDeleteTask = (id) => {
    axios.delete(`${baseUrl}/${id}`)
          .then(response => {
            console.log(response.data);
          })
          .catch(error => {
            console.log(error);
          });
  };

  const tasks = toDos.map((task, key) => {
    return (
      <Task
        text={task.taskName}
        id={task.id}
        deleteFunc={handleDeleteTask}
        key={key}
        editTask={() => handleEditTask(task.id, task.taskName, task.type)}
        type={task.type}
      />
    );
  });


  

  return (
    <div className="app-container flex-col bg-lime-200 p-10 jus">
      <Title />

      <div className="flex-col lg:max-w-4xl sm:min-w-1xl justify-center w-full h-full">
        <div className="flex add-task-container mx-10 my-10 w-full h-full">
          <TaskInput handleInput={handleInput} inputState={inputValue} />

          <AddButton
           addTask={handleAddTask}
            taskName={inputValue}
            taskType={type}
          />


          {/* <TaskTypeDropDown handleChange={handleChangeDropdown} />    */}
          <div className="text-2xl p-4 basis-1/5">
            <label>Type: </label>

            <select
              name="type"
              id="taskType"
              value={type}
              onChange={handleChangeDropdown}
            >
              {taskTypes.map((type, key) => {
                return <option key={key}value={type}> {type} </option>;
              })}
            </select>
          </div>
        </div>

        <TodoList tasks={tasks} />
        <Dropdown />

      </div>
    </div>
  );
}

export default App;
