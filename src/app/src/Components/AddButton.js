import React from "react";

export default function AddButton(props) {
  return (
    <button
      className="bg-cyan-400 px-8 rounded-full text-xl font-bold hover:bg-cyan-300 basis-1/5 m-3"
      onClick={() => props.addTask()}
    >
      +
    </button>
  );
}
