import React, { useEffect, useState } from "react";

function DropDown(props) {
  const citiesObj = [
    {
      id: 1,
      city: "Pune",
      stateId: 2,
    },
    {
      id: 2,
      city: "Daman",
      stateId: 3,
    },
    {
      id: 3,
      city: "Diu",
      stateId: 3,
    },
    {
      id: 4,
      city: "Mumbai",
      stateId: 2,
    },
    {
      id: 5,
      city: "Nashik",
      stateId: 2,
    },
    {
      id: 6,
      city: "Kolhapur",
      stateId: 2,
    },
    {
      id: 7,
      city: "Solapur",
      stateId: 2,
    },
    {
      id: 8,
      city: "Wardha",
      stateId: 2,
    },
    {
      id: 9,
      city: "Ahmedabad",
      stateId: 1,
    },
    {
      id: 10,
      city: "Gandhinagar",
      stateId: 1,
    },
    {
      id: 11,
      city: "Surat",
      stateId: 1,
    },
    {
      id: 12,
      city: "Rajkot",
      stateId: 1,
    },
    {
      id: 13,
      city: "Jamnagar",
      stateId: 1,
    },
    {
      id: 14,
      city: "New Delhi",
      stateId: 4,
    },
    {
      id: 15,
      city: "Hastsal",
      stateId: 4,
    },
    {
      id: 16,
      city: "Dallupura",
      stateId: 4,
    },
    {
      id: 17,
      city: "Alipur",
      stateId: 4,
    },
    {
      id: 18,
      city: "Jafrabad",
      stateId: 4,
    },
  ];

  const statesObjs = [
    {
      id: 1,
      state: "Gujarat",
    },
    {
      id: 2,
      state: "Maharastra",
    },
    {
      id: 3,
      state: "Daman & Diu",
    },
    {
      id: 4,
      state: "Delhi",
    },
  ];

  const [states, setStates] = useState([]);
  const [cities, setCities] = useState([]);

  useEffect(() => {
    setStates(statesObjs);
  }, []);

  const handleChangeState = (id) => {
    const cities = citiesObj.filter((city) => city.stateId == id);
    setCities(cities);
  };

  return (
    <div>
      <select
        name="states"
        id="ddlState"
        onChange={(event) => handleChangeState(event.target.value)}
      >
        <option value={0}>Select State</option>
        {states.map((state, key) => {
          return (
            <option key={key} value={state.id}>
              {" "}
              {state.state}{" "}
            </option>
          );
        })}
      </select>

      <br></br>

      <select name="cities" id="ddlCity">
        <option value={0}>Select City</option>
        {cities.map((city, key) => {
          return (
            <option key={key} value={city.id}>
              {" "}
              {city.city}{" "}
            </option>
          );
        })}
      </select>
    </div>
  );
}

export default DropDown;
