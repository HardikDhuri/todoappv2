import React from "react";

export default function TaskInput(props) {
  return (
    <input
      id="toDoInput"
      type="text"
      placeholder="What do you want to get done?"
      className="p-2 mr-3 rounded-md w-3/5 font-bold basis-3/5"
      onChange={props.handleInput}
      value={props.inputState}
    />
  );
}
