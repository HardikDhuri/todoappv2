import React from "react";

const TaskTypeDropDown = (props) => {
  return (
    <div className="text-2xl p-4 basis-1/5">
      <label>Type: </label>

      <select name="type" id="type" onChange={props.handleChange}>
        {taskTypes.map((type) => {
          return <option value={type}> {type} </option>;
        })}
      </select>
    </div>
  );
};

export default TaskTypeDropDown;
