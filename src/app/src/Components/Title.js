import React from "react";

export default function Title() {
  return (
    <h1 className="text-3xl font-extrabold text-center">Daily Goal Tracker</h1>
  );
}
