import React from "react";

export default function TodoList(props) {
  return (
    <div className="list-container bg-lime-400 mx-10 my-5 rounded-lg">
      <div className="p-5 flex-col w-full h-full">{props.tasks}</div>
    </div>
  );
}
