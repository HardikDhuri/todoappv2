import React, { useState } from "react";

function Task(props) {
  const [isDone, setIsDone] = useState(false);

  const getBackgroundColor = (taskType) => {
    switch (taskType) {
      case "work":
        return " bg-cyan-200 ";

      case "personal":
        return " bg-amber-200 ";

      default:
        return " bg-violet-200 ";
    }
  };

  const handleCheck = () => {
    setIsDone(!isDone);
  };

  return (
    <div
      className={
        "flex mb-3 rounded-xl p-2 items-center justify-between" +
        getBackgroundColor(props.type)
      }
    >
      <p
        className={
          "text-2xl text-left basis-6/9" + (isDone ? " line-through " : "")
        }
      >
        {props.text}
      </p>

      <input
        type="checkbox"
        className="w-10 h-10 rounded basis-1/9 hover:bg-slate-400"
        onChange={handleCheck}
      />

      <button
        className="text-white font-bold p-2 bg-red-500 hover:bg-red-700 rounded-lg my-2 basis-1/9"
        onClick={() => props.deleteFunc(props.id)}
      >
        {isDone ? "Smash it" : "See u later"}
      </button>

      <button
        className="text-white font-bold p-2 bg-sky-300 hover:bg-sky-700 rounded-lg my-2 basis-1/9 "
        onClick={props.editTask}
      >
        Edit
      </button>
    </div>
  );
}

export default Task;
